import {getValueOrEmpty, json, status} from "./utils";
import {domenUrl} from "./app";

export class Person {

    //------------- Сетевые запросы

    static getAll() {
        const request = new Request(`${domenUrl}/v1/persons`)
        return fetch(request)
            .then(status)
            .then(json)
            .catch(error => console.log('Request failed', error))
    }

    static getById(id) {
        const request = new Request(`${domenUrl}/v1/persons/${id}`)
        return fetch(request)
            .then(status)
            .then(json)
            .catch(error => console.log('Request failed', error))
    }

    static update(person) {
        const request = new Request(
            (person.id !== "") ? `${domenUrl}/v1/persons/${person.id}`
                : `${domenUrl}/v1/persons`)
        return fetch(request, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(person)
        })
            .then(status)
            .then(json)
            .catch(error => console.log('Request failed', error))
    }

    static delete(id) {
        const request = new Request(`${domenUrl}/v1/persons/${id}`)
        return fetch(request, {
            method: 'DELETE'
        })
            .then(status)
            .catch(error => console.log('Request failed', error))
    }

    //------------- Визуализация

    static addCardList(persons) {
        if (persons.length) {
            const cards = document.getElementById('js-personalCards')
            persons.map(person => {
                const card = fillCardListHtml(person)
                cards.append(card)
            })
        }
    }

    static cleanCardList() {
        const cards = document.getElementById('js-personalCards')
        cards.innerHTML = ``
    }

    static updateCardList(person) {
        const existCards = document.getElementById('js-personalCards').querySelectorAll(`[data-id="${person.id}"]`)
        if (existCards.length === 0) {
            Person.addCardList(Array.of(person))
        } else {
            existCards.forEach(card => fillCardListHtml(person, card))
        }
    }

    static deleteCardList(id) {
        const cardsList = document.getElementById('js-personalCards')
        const cards = cardsList.querySelectorAll(`[data-id="${id}"]`)
        cards.forEach(card => cardsList.removeChild(card))
    }

    static readDetail() {
        const detailsForm = document.getElementById('js-detail-form-person')
        return {
            id: detailsForm.getAttribute('data-id'),
            surname: detailsForm.querySelector('#surname').value,
            name: detailsForm.querySelector('#name').value,
            patronymic: detailsForm.querySelector('#patronymic').value
        }
    }

    static showDetailEmpty() {
        fillDetailForm({})
    }

    static showDetailById(id) {
        Person.getById(id)
            .then(Person.showDetail)
    }

    static showDetail(person) {
        fillDetailForm(person)
    }
}

$(function () {
    $(document).on("click", '#jq-createBtnPerson', Person.showDetailEmpty)
    $(document).on("click", '#jq-updateBtnPerson', () => {
        const personDetail = Person.readDetail()
        Person.update(personDetail)
            .then(person => {
                Person.showDetail(person)
                Person.updateCardList(person)
            })
    })
    $(document).on("click", '#jq-deleteBtnPerson', () => {
        const person = Person.readDetail()
        Person.delete(person.id)
            .then(() => {
                Person.deleteCardList(person.id)
                clearDetailForm(person.id)
            })
    });
    $(document).on("click", '.jq-click-person-card', (event) => {
        Person.showDetailById(event.currentTarget.getAttribute("data-id"))
    });
});

function fillCardListHtml(person, existCard) {
    const item = (existCard != null)
        ? existCard : document.createElement("div")
    item.setAttribute("data-id", person.id)
    item.classList.add('jq-click-person-card') //для событий при нажатии
    item.innerHTML =
        `
        <br>
        <div class="card">
            <div id="surname" class="mui--text-headline">${person.surname}</div>
            <div class="mui--text-black-54">${person.name} ${person.patronymic}</div>
        </div>
        `
    return item
}

function fillDetailForm(person) {
    const detailForm = document.getElementById('js-detailsPersonalCard')
    detailForm.innerHTML =
        `
                    <form id="js-detail-form-person" data-id="${getValueOrEmpty(person.id)}" class="mui-form">
                      <legend>Основная информация</legend>
                      <div class="mui-textfield mui-textfield--float-label">
                        <input id="surname" type="text" value="${getValueOrEmpty(person.surname)}">
                        <label>Фамилия</label>
                      </div>
                      <div class="mui-textfield mui-textfield--float-label">
                        <input id="name" type="text" value="${getValueOrEmpty(person.name)}">
                        <label>Имя</label>
                      </div>
                      <div class="mui-textfield mui-textfield--float-label">
                        <input id="patronymic" type="text" value="${getValueOrEmpty(person.patronymic)}">
                        <label>Отчество</label>
                      </div>
                      <button id="jq-updateBtnPerson" type="button" class="mui-btn mui-btn--small mui-btn--primary">Сохранить</button>
                      <button id="jq-deleteBtnPerson" type="button" class="mui-btn mui-btn--small mui-btn--primary">Удалить</button>
                    </form>
                `
}

function clearDetailForm() {
    const detailForm = document.getElementById('js-detail-form-person')
    detailForm.innerHTML = ''
}

export function personalCardsHtml() {
    return `
    <div class="flex content-rrr">
        <div class="left-content-rrr">
            <div class="box">
               <strong class="box-inner"> Список карточек </strong> 
               <button id='jq-createBtnPerson' class="box-inner mui-btn mui-btn--small-rrr mui-btn--primary mui-btn--fab">+</button>
            </div>
            <div class="mui-divider"></div>
            <div class="split"> 
               <div id="js-personalCards"></div>
            </div>
        </div>
        <div id="js-detailsPersonalCard" class="split right-content-rrr mui--divider-left"></div>
    </div>
    `
}