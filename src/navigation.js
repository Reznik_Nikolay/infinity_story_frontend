import {Person, personalCardsHtml} from "./person";
import {visualizationTreeHtml} from "./visual/visualization";

$(function () {
    $(document).on("click", '.link-main-rrr', (event) => setNavigationByLink(event.target));
    $(document).on("click", '.link-personal-cards-rrr', (event) => setNavigationByLink(event.target));
    $(document).on("click", '.link-family-list-rrr', (event) => setNavigationByLink(event.target));
    $(document).on("click", '.link-visualization-tree-rrr', (event) => setNavigationByLink(event.target));
});

function setNavigationByLink(linkElement) {
    location.hash = linkElement.getAttribute("href")
    fillBoxPage()
}

export function fillBoxPage() {
    const containerFill = document.getElementById('fill-container-rrr')
    clearBox(containerFill)
    fillBox(containerFill)
}

function clearBox(value) {
    value.innerHTML = "";
}

function fillBox(value) {
    const div = document.createElement('div')

    if (location.hash === "#")
        div.innerHTML = mainHtml()
    else if (location.hash === "#personal-cards") {
        div.innerHTML = personalCardsHtml()
        Person.getAll()
            .then(Person.addCardList)
    }
    else if (location.hash === "#visualization-tree") {
        div.innerHTML = visualizationTreeHtml()
    }
    else if (location.hash === "#family-list")
        div.innerHTML = familyListHtml()
    else
        div.innerHTML = mainHtml()

    value.append(div)
}

function mainHtml() {
    return `
            <h1>История бесконечности</h1>
            <p>Сайт предназначен для хранения генеалогического древа каждой ячейки семьи.</p>
            <p>
                <strong>Возможности:</strong>
                <ul>
                    <li>Пока нет</li>
                </ul>
            </p>
            <p>
                <strong>Будущие доработки:</strong>
            <ul>
                <li>Заведение карточек личностей</li>
                <li>Добавление фотографий</li>
                <li>Ведение исторической ленты</li>
                <li>Визуализация генеалогического древа</li>
            </ul>
            </p>
        `
}

function familyListHtml() {
    return `
    <div class="mui--text-black-54 mui--text-body2">Список семей</div>
    <div class="mui-divider"></div>
    <br>
    <p>Тут будет список</p>
    `
}
