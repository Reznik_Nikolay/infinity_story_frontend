import $ from 'jquery';
import 'jquery-ui';
import {Box} from "./Box";
import {Person} from "../person";
import {getPanelRight} from "../utils";

const boxes = [];
const links = [];
let link = null;
let canvasEl = null
let canvasContext = null
let canvasXY = null;

export function visualBodyHtml() {
    document.getElementById('js-visualization-body')
        .innerHTML = `
        <strong>Управление:</strong>  <br>
        MouseMove -- перемещение коробок. <br>
        Shift + Click -- добавить коробку. <br>
        Ctrl (на коробке) + MouseMove -- провести линию.<br><br>
        <button id="jq-switchPanelRightBtn">Боковая панель</button>
        <hr>
        <div id="droppablePerson"><canvas id="canvas" width="600" height="600"></canvas></div>
        `
}

export function visualizationTreeHtml() {
    return `
     <div class="flex content-rrr">
        <div class="left-content-rrr">
            <div class="box">
               <strong class="box-inner"> Список деревьев </strong>
               <button id='jq-createBtnTree' class="box-inner mui-btn mui-btn--small-rrr mui-btn--primary mui-btn--fab">+</button>
            </div>
            <div class="mui-divider"></div>
            <div class="split">
               <div id="js-treeCards"></div>
            </div>
        </div>
        <div id="js-visualization-body" class="split right-content-rrr mui--divider-left"></div>

        <!--боковая панель-->
        ${getPanelRight()}
    </div>
    `
}

$(function () {
    $(document).on("click", '#jq-createBtnTree', () => {
        visualBodyHtml()
        canvasEl = $("#canvas").get(0)
        canvasContext = canvasEl.getContext('2d')
    });

    $(document).on("click", '#jq-switchPanelRightBtn', () => {
        switchPanelRight()
    });
    $(document).on("click", '.side-button-close', () => {
        switchPanelRight()
    });

    $(document).on("mousedown", '#canvas', function (e) {
        const position = getCoordinates($(this), e)
        const box = getBoxByCoordinates(position.x, position.y);
        if (e.shiftKey) {
            const box = new Box(position.x, position.y, 50, 50);
            box.name = boxes.length.toString()
            boxes.push(box)
            draw_all();
            return;
        }
        if (!box)
            return;
        if (e.ctrlKey) {
            link = {box};
            return;
        }
        getBoxByCoordinates(position.x, position.y).on_mouse_down(e);
    });

    $(document).on("mousemove", '#canvas', function (e) {
        if (link) {
            link.to = getCoordinates($(this), e);
        } else {
            for (const box of boxes) {
                box.on_mouse_move(e);
                draw_boxes()
            }
        }
        draw_all();
    });

    $(document).on("mouseup", '#canvas', function (e) {
        const coordinates = getCoordinates($(this), e)
        const box = getBoxByCoordinates(coordinates.x, coordinates.y);
        if (link) {
            if (link.box === box || !box) {
                link = null;
                return;
            }
            links.push({from: link.box, to: box});
        } else {
            for (const box of boxes)
                box.on_mouse_up(e);
        }
        link = null;
    });
})

function switchPanelRight() {
    const panel = document.querySelector('.side-panel')
    if (panel.classList.contains('active')) {
        document.querySelector('.side-panel').classList.remove('active');
        Person.cleanCardList()
    } else {
        document.querySelector('.side-panel').classList.add('active');
        Person.getAll()
            .then(Person.addCardList)

        $("#canvas").droppable({
            accept: '.jq-click-person-card',
            drop: function (event, ui) {
                const position = getCoordinates($(this), event)
                const box = new Box(position.x, position.y, 50, 50);
                box.name = ui.helper.get(0).querySelector('#surname').innerText
                boxes.push(box)
                draw_all();
            }
        });

        $(".side-panel").on("DOMNodeInserted", ".jq-click-person-card", function () {
            $(this).draggable({
                helper: 'clone'
            });
        });
    }
}

function getBoxByCoordinates(x, y) {
    for (const box of boxes) {
        if (x >= box.x && x < box.x + box.w && y >= box.y && y < box.y + box.h) {
            return box;
        }
    }
    return null;
}

function getCoordinates(canvasEl, e) {
    if (!canvasXY) {
        canvasXY = {}
        const position = canvasEl.offset();
        canvasXY.x = position.left.toFixed(0)
        canvasXY.y = position.top.toFixed(0)
    }
    const x = e.pageX - canvasXY.x;
    const y = e.pageY - canvasXY.y;
    return {x: x, y: y};
}

function draw_boxes() {
    for (const box of boxes) {
        box.draw(canvasContext);
    }
}

function draw_links() {
    for (const link of links) {
        const centerBoxFrom = link.from.center()
        const centerBoxTo = link.to.center();
        canvasContext.beginPath();
        canvasContext.moveTo(centerBoxFrom.x, centerBoxFrom.y);
        canvasContext.lineTo(centerBoxTo.x, centerBoxTo.y);
        canvasContext.stroke();
    }
}

function draw_link() {
    const centerBox = link.box.center();
    canvasContext.beginPath();
    canvasContext.moveTo(centerBox.x, centerBox.y);
    canvasContext.lineTo(link.to.x, link.to.y);
    canvasContext.stroke();
}

function draw_all() {
    canvasContext.clearRect(0, 0, canvasEl.width, canvasEl.height);
    draw_links();
    draw_boxes();
    if (link)
        draw_link();
}