export class Box {
    constructor(x, y, w, h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.drag = null;
    }

    on_mouse_down(e) {
        this.drag = {
            oldx: this.x, oldy: this.y, // old position of box
            msx: e.clientX, msy: e.clientY // mouse start x, y
        };
    }

    on_mouse_move(e) {
        if (!this.drag) return;
        const dx = e.clientX - this.drag.msx, dy = e.clientY - this.drag.msy;
        this.x = this.drag.oldx + dx;
        this.y = this.drag.oldy + dy;
    }

    on_mouse_up(e) {
        this.drag = null;
    }

    draw(canvasContext) {
        canvasContext.save();
        canvasContext.fillRect(this.x, this.y, this.w, this.h);
        canvasContext.font = "30px Georgia";
        canvasContext.fillStyle = "white";
        canvasContext.fillText(this.name, this.x + 10, this.y + 30);
        canvasContext.restore();
    }

    center() {
        return {x: this.x + Math.floor(this.w / 2), y: this.y + Math.floor(this.h / 2)};
    }
}